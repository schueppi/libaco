# LiBaCo

Linux basic configuration

## Getting started

### This repository

Run ansible playbooks/main.yml

### Via ansible-galaxy

Create requirements.yml with following content:
```
---
roles:
  - name: libaco
    src: https://gitlab.com/schueppi/libaco.git
    version: main
    scm: git
```

Install libaco role via ansible-galaxy install e.g.:
```
docker run --rm -v $PWD:/ansible -v $PWD/test:/root/.ansible/roles/ envot/ansible-dockerized ansible-galaxy install -r requirements.yml
```


## License
see [LICENSE](LICENSE).
