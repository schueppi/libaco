#!/bin/bash
echo "You must be logged in:"
glab auth status
echo 'If you are not logged in correctly, use "glab auth login"'
export GITLAB_URI=${2:-https://gitlab.com}
glab repo clone -g $1 -p --paginate

